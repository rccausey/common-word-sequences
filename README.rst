=====================
Common word sequences
=====================

This project provides a script, common_phrases.py, that will tally the
100 most common three word phrases in a given input.

=====
Usage
=====

The script can be executed by providing a list of files like so:

.. code-block:: console

    ./common_phrases.py /path/to/my/file.txt /path/to/my/other/file.txt

The script also accepts input from stdin:

.. code-block:: console

    cat /path/to/my/file.txt | ./common_phrases.py

These methods cannot both be used at the same time. If a file path is provided,
stdin is ignored.

=============
Running tests
=============

In order to run tests, one needs to set up the dev environment. This project
uses pipenv to manage dependencies and virtualenvs.

First, install all of the dev dependencies:

.. code-block:: console

    pipenv sync --dev

Then, run the tests using pytest:

.. code-block:: console

    pipenv run pytest -vvv .

============
Known issues
============

This script will currently match numbers as "words" in a given input. I wasn't
able to resolve this satisfactorily within the time alloted.

This isn't winning any awards for speed on the large text files.

Files are not processed in parallel.
