#!/usr/bin/env python3

import sys
import argparse
from collections import Counter
import re
import itertools

def window(seq, window_size):
    """Returns a sliding window (of width window_size) over data from the iterable"

    s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...

    Taken from stack overflow + old python itertools docs.
    https://stackoverflow.com/a/6822773
    """
    it = iter(seq)
    result = tuple(itertools.islice(it, window_size))
    if len(result) == window_size:
        yield result
    for elem in it:
        result = result[1:] + (elem,)
        yield result


if __name__ == "__main__":
    # Allow parsing file names from the command line
    parser = argparse.ArgumentParser(
        description = """
            This command will read the provided input and list the 100 most common 3 word phrases.
            It is case insensitive, and so will treat "foo bar baz" the same as "FOO BAR BAZ".
        """
    )
    # Parse input as files, but if none are provided default to stdin.
    parser.add_argument(
        "input_file",
        nargs = "*",
        type = argparse.FileType(mode = "r"),
        default = [sys.stdin],
        help = "Optional sets of files to read words from. If none are provided the program will try to read from stdin."
    )
    parsed_args = parser.parse_args()
    # Indicate if we're reading from stdin because if this program is invoked without being piped to
    # or any args, it will sit waiting for input.
    if parsed_args.input_file[0] is sys.stdin:
        print("Reading from stdin.")

    # Flatten the lines of each file into one iterable
    all_lines = itertools.chain.from_iterable(
        input_file.readlines() for input_file in parsed_args.input_file
    )
    # Read each line from the input file and get all the words and flatten them into one iterable.
    all_words = itertools.chain.from_iterable(
        # This generator expression returns all words found in a given line.
        # The resulting string is casefolded so that case is ignored.
        # The regex accounts for apostrophes, like in `don't`.
        (match[0].casefold() for match in re.finditer(r"\w+('\w+)?", line, flags = re.IGNORECASE))
        for line in all_lines
    )

    # Now use a Counter to count all of the 3 word tuples.
    # We use a sliding window of size 3 to get overlapping sets of words.
    counted_groups = Counter(window(all_words, 3))
    # print the top 100, 3 word phrases
    print("Printing top 100, 3 word phrases...\n")
    for phrase, count in counted_groups.most_common(100):
        print(f"{' '.join(phrase)} - {count}")
