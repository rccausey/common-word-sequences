from pathlib import Path
import pytest
from common_phrases import window

@pytest.mark.parametrize(
    "window_size, test_input, expected_output",
    (
        (1, ("foo", "bar", "baz"), (("foo",), ("bar",), ("baz",))),
        (2, ("foo", "bar", "baz"), (("foo", "bar"), ("bar", "baz"))),
        (3, ("foo", "bar", "baz"), (("foo", "bar", "baz"),)),
        (4, ("foo", "bar", "baz"), tuple()),
    )
)
def test_window(window_size, test_input, expected_output):
    """Test that window works as expected, creating a sliding window.

    If the window size is larger than the number of elements, then there
    should be no window returned.
    """
    assert expected_output == tuple(window(test_input, window_size))

@pytest.mark.parametrize(
    "test_cmd",
    (
        "cat ./test_files/hamlet.txt | ./common_phrases.py",
        "./common_phrases.py ./test_files/hamlet.txt",
    ),
)
def test_hamlet(host, test_cmd):
    """Expect hamlet to parse the same every time."""
    result = host.run(test_cmd)
    assert result.rc == 0
    assert Path("./test_files/hamlet_expected_result.txt").resolve(strict = True).read_text() in result.stdout

def test_hamlet_multiple_files(host):
    """Test that we can pass multiple files. In this case we pass hamlet twice."""
    result = host.run("./common_phrases.py ./test_files/hamlet.txt ./test_files/hamlet.txt")
    assert result.rc == 0
    # Double the hamlets, double the ham lords.
    assert "my lord ham - 126" in result.stdout

@pytest.mark.parametrize(
    "test_cmd",
    (
        "cat ./test_files/case_insensitive.txt | ./common_phrases.py",
        "./common_phrases.py ./test_files/case_insensitive.txt",
    ),
)
def test_case_insensitive(host, test_cmd):
    """Ensure that case insensitivity works as expected."""
    result = host.run(test_cmd)
    assert result.rc == 0
    assert "foo bar baz - 2" in result.stdout
    assert "der fluss huh - 2" in result.stdout

@pytest.mark.parametrize(
    "test_cmd",
    (
        "cat ./test_files/ignore_non_word_chars.txt | ./common_phrases.py",
        "./common_phrases.py ./test_files/ignore_non_word_chars.txt",
    ),
)
def test_ignore_non_word_chars(host, test_cmd):
    """Ensure that non word characters are ignored."""
    result = host.run(test_cmd)
    assert result.rc == 0
    assert "foo bar baz - 1" in result.stdout

@pytest.mark.parametrize(
    "test_cmd",
    (
        "cat ./test_files/empty.txt | ./common_phrases.py",
        "./common_phrases.py ./test_files/empty.txt",
    ),
)
def test_empty(host, test_cmd):
    """Ensure that we can handle an empty file."""
    result = host.run(test_cmd)
    assert result.rc == 0

@pytest.mark.parametrize(
    "test_cmd",
    (
        "cat ./test_files/contractions.txt | ./common_phrases.py",
        "./common_phrases.py ./test_files/contractions.txt",
    ),
)
def test_contractions(host, test_cmd):
    """Ensure that contractions are handled properly and aren't broken apart."""
    result = host.run(test_cmd)
    assert result.rc == 0
    for word in ("shouldn't", "couldn't", "won't", "can't", "don't"):
        assert word in result.stdout

    # Make sure trailing and leading apostrophes are ignored.
    assert "can't don't don" in result.stdout

@pytest.mark.parametrize(
    "test_cmd",
    (
        "cat ./test_files/large_file.txt | ./common_phrases.py",
        "./common_phrases.py ./test_files/large_file.txt",
    ),
)
def test_large_file(host, test_cmd):
    """Ensure that we don't choke on a large text file, approximately 20 megabytes in this case."""
    result = host.run(test_cmd)
    assert result.rc == 0
